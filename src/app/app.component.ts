import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from './dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, { // open dialog
      width: '250px', // setting width dialog
      data: "data string" // kirim data ke dialogComponent
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed'); // akan tampil setelah dialog di close
    });
  }
}
